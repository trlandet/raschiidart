// @dart=2.9
// TODO: The code needs to be reworked for safe nullable behaviour in dart >= 2.10 

abstract class WaveModel {
  double length, height, depth;
  double k, omega, c;
  double g = 9.81;

  List<double> velocity(double x, double z);

  List<double> eta(List<double> x);
  String info() => '';
  String get_errors() => '';
  String get_warnings() => '';
}
